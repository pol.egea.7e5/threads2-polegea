﻿using System;

namespace Threads2PolEgea
{
    using System;
    using System.Threading;

    

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nExercici1:");
            Exercici1 ex1 = new Exercici1("Una vegada hi havia un gat","En un lugar de la Mancha","Once upon a time");
            Console.WriteLine("\nExercici2:");
            Exercici2 nevera = new Exercici2(new Random().Next(0,4));
            Thread threadAnitta = new Thread(() => nevera.OmplirNevera("Anitta"));
            Thread threadBadBunny = new Thread(() => nevera.BeureCervesa("Bad Bunny"));
            Thread threadLilNasX = new Thread(() => nevera.BeureCervesa("Lil Nas X"));
            Thread threadManuelTurizo = new Thread(() => nevera.BeureCervesa("Manuel Turizo"));

            threadAnitta.Start();
            threadAnitta.Join();
            threadBadBunny.Start();
            threadBadBunny.Join();
            threadLilNasX.Start();
            threadLilNasX.Join();
            threadManuelTurizo.Start();
            threadManuelTurizo.Join();

            Console.WriteLine("Final del programa.");
        }
        class Exercici1
        {
            public Exercici1(string frase1,string frase2,string frase3)
            {
                Thread first = new Thread(() => CountFrases(frase1));
                Thread second = new Thread(() => CountFrases(frase2));
                Thread third = new Thread(() => CountFrases(frase3));
                first.Start();
                first.Join();
                second.Start();
                second.Join();
                third.Start();
                third.Join();

            }
            public void CountFrases(string frase)
            {
                string[] paraules = frase.Split(" ");
                foreach (string paraula in paraules)
                {
                    Console.Write(paraula+" ");
                    Thread.Sleep(1000);
                }
                Console.WriteLine();
            }
        }
        class Exercici2
        {
            private int quantitatCerveses;
            private readonly int capacitatMaxima = 9;
            private readonly Random random = new Random();

            public Exercici2(int quantitatInicial)
            {
                quantitatCerveses = quantitatInicial;
            }

            public void OmplirNevera(string nomPersona)
            {
                int quantitatAIncrementar = random.Next(1, 7);
                while ((quantitatCerveses + quantitatAIncrementar) > capacitatMaxima)
                {
                    quantitatAIncrementar--;
                }
                quantitatCerveses += quantitatAIncrementar;
                if (quantitatAIncrementar > 0) Console.WriteLine("{0} ha omplert la nevera amb {1} cerveses.", nomPersona, quantitatAIncrementar);
                else Console.WriteLine("No s'ha pogut omplir la nevera ja que és igual a la quantitat màxima");
            }

            public void BeureCervesa(string nomPersona)
            {
                if (quantitatCerveses == 0)
                {
                    Console.WriteLine("La nevera està buida, {0} no pot beure cap cervesa.", nomPersona);
                }
                else
                {
                    int quantitatARestar = random.Next(1, 7);
                    while (quantitatCerveses - quantitatARestar < 0)
                    {
                        quantitatARestar--;
                    }
                    quantitatCerveses -= quantitatARestar;
                    Console.WriteLine("{0} ha begut {1} cerveses i ara queden {2} cerveses a la nevera.", nomPersona, quantitatARestar, quantitatCerveses);
                }
            }
        }
    }
}
